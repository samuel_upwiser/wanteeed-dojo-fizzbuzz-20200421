import { expect } from 'chai'
import say from '../src/fizzBuzz'

describe('FizzBuzz', () => {
    describe('Normal numbers', () => {
        it('should say "1" for 1', () => {
            expect(say(1)).to.equal("1")
        })
        it('should say "2" for 2', () => {
            expect(say(2)).to.equal("2")
        })
    })


    describe('Fizz', () => {
        it('should say "Fizz" for 3', () => {
            expect(say(3)).to.equal("Fizz")
        })

        it('should say "Fizz" for 6', () => {
            expect(say(2 * 3)).to.equal("Fizz")
        })
    })

    describe('Buzz', () => {
        it('should say "Buzz" for 5', () => {
            expect(say(5)).to.equal("Buzz")
        })
        it('should say "Buzz" for 10', () => {
            expect(say(5 * 2)).to.equal("Buzz")
        })
    })

    describe('FizzBuzz rule', () => {
        it('should say "FizzBuzz" for 15', () => {
            expect(say(3 * 5)).to.equal("FizzBuzz")
        })

    })

})
