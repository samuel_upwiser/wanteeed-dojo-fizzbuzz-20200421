const isMultipleOf = divisor => number => number % divisor === 0

const isMultipleOf3 = isMultipleOf(3)
const isMultipleOf5 = isMultipleOf(5)

const say = (number) => {
    let result = ''
    if (isMultipleOf3(number)) {
        result += "Fizz"
    }
    if (isMultipleOf5(number)) {
        result += "Buzz"
    }
    return result.length === 0
        ? number.toString()
        : result
}

export default say
